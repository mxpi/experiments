import os
import sys
import stat
import hashlib

BLOCKSIZE = 65536

file_array = []
file_hash = {}

def coroutine(func):
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        next(cr)
        return cr
    return start

def get_base_dir():
    if len(sys.argv) > 1:
        base_dir = sys.argv[1]
    else:
        base_dir = os.getcwd()
    if base_dir != '/':
        base_dir = base_dir.rstrip('/')
    if not os.path.isdir(base_dir):
        print("Directory %s doesn't exist! Aborting" % base_dir)
        sys.exit(1)
    return base_dir

def get_md5hash(file):
    hasher = hashlib.md5()
    with open(file, 'rb') as f:
        buf = f.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = f.read(BLOCKSIZE)
    return hasher.hexdigest()

def find_files():
    base_dir = get_base_dir()
    for root, dirs, files, _ in os.fwalk(base_dir):
        if root.find(".git") == -1:
            for file in files:
                full_path = '/'.join((root, file))
                if not os.path.islink(full_path): # skip symlinks
                    stats = os.stat(full_path)
                    if not stat.S_ISSOCK(stats[0]): # skip sockets
                        yield full_path
    yield 'Done'

def data_pipe(source, target):
    while True:
        src = next(source)
        if src == 'Done':
            target.send('Done')
            return
        if src:
           target.send(src)

@coroutine
def calculate_md5hash(target):
    while True:
        file = (yield) 
        if file and file != 'Done':
            target.send((get_md5hash(file), file))

@coroutine
def populate_hash_table():
    global file_hash, file_array
    while True:
        md5, file = (yield)
        if md5 in file_hash.keys():
            file_hash[md5].append(file)
            if md5 not in file_array:
                file_array.append(md5)
        else:
            file_hash[md5] = [file]


if __name__ == "__main__":

    data_pipe(find_files(), calculate_md5hash(populate_hash_table()))

    for h in file_array:
        print(file_hash[h])