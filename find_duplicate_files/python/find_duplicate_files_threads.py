import os
import sys
import stat
import hashlib
from threading import Thread
from queue import Queue

BLOCKSIZE = 65536

file_array = []
file_hash = {}

class ClosableQueue(Queue):
    SENTINEL = object()

    def close(self):
        self.put(self.SENTINEL)

    def __iter__(self):
        while True:
            item = self.get()
            try:
                if item is self.SENTINEL:
                    return  # Cause the thread to exit
                yield item
            finally:
                self.task_done()


class StoppableWorker(Thread):
    def __init__(self, func, in_queue, out_queue):
        super().__init__()
        self.func = func
        self.in_queue = in_queue
        self.out_queue = out_queue
        self.work_done = 0

    def run(self):
        for item in self.in_queue:
            if item == 'Done':
                self.in_queue.close()
            result = self.func(item)
            self.out_queue.put(result)


file_queue = ClosableQueue()
populate_hash_table_queue = ClosableQueue()
display_queue = ClosableQueue()
done_queue = ClosableQueue()

def get_base_dir():
    if len(sys.argv) > 1:
        base_dir = sys.argv[1]
    else:
        base_dir = os.getcwd()
    if base_dir != '/':
        base_dir = base_dir.rstrip('/')
    if not os.path.isdir(base_dir):
        print("Directory %s doesn't exist! Aborting" % base_dir)
        sys.exit(1)
    return base_dir

def find_files():
    base_dir = get_base_dir()
    for root, dirs, files, _ in os.fwalk(base_dir):
        if root.find(".git") == -1:
            for file in files:
                full_path = '/'.join((root, file))
                if not os.path.islink(full_path): # skip symlinks
                    stats = os.stat(full_path)
                    if not stat.S_ISSOCK(stats[0]): # skip sockets
                        yield full_path
    yield 'Done'

def get_md5hash(file):
    if file == 'Done':
        return 'Done'
    hasher = hashlib.md5()
    with open(file, 'rb') as f:
        buf = f.read(BLOCKSIZE)
        while len(buf) > 0:
            hasher.update(buf)
            buf = f.read(BLOCKSIZE)
    return (hasher.hexdigest(), file)

def populate_hash_table(pair):
    global file_hash, file_array
    if pair != 'Done':
        md5, file = pair
        if md5 in file_hash.keys():
            file_hash[md5].append(file)
            if md5 not in file_array:
                file_array.append(md5)
        else:
            file_hash[md5] = [file]


threads = [
    StoppableWorker(get_md5hash, file_queue, populate_hash_table_queue),
    StoppableWorker(populate_hash_table, populate_hash_table_queue, done_queue),
]

for thread in threads:
    thread.start()

for f in find_files():
    file_queue.put(f)

file_queue.join()
populate_hash_table_queue.join()

for h in file_array:
    print(file_hash[h])