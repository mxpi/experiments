#include "queue.h"
#include "dbg.h"

Queue *Queue_create()
{
    return calloc(1, sizeof(Queue));
}

void Queue_destroy(Queue *queue)
{
    QUEUE_FOREACH(queue, cur)
        if (cur->prev) {
            free(cur->prev);
        }

    free(queue->last);
    free(queue);
}

void Queue_clear(Queue *queue)
{
    QUEUE_FOREACH(queue, cur)
        free(cur->value);
}

void Queue_clear_destroy(Queue *queue)
{
    Queue_clear(queue);
    Queue_destroy(queue);
}

void Queue_send(Queue *queue, void *value)
{
    QueueNode *node = calloc(1, sizeof(QueueNode));
    check_mem(node);

    node->value = value;

    if (queue->last == NULL) {
        queue->first = node;
        queue->last = node;
    } else {
        queue->last->next = node;
        node->prev = queue->last;
        queue->last = node;
    }

    queue->count++;

    error:
        return;
}

void *Queue_recv(Queue *queue)
{
    QueueNode *node = queue->first;
    return node != NULL ? Queue_remove(queue, node) : NULL;
}

void *Queue_remove(Queue *queue, QueueNode *node)
{
    void *result = NULL;

    check(queue->first && queue->last, "Queue is empty");
    check(node, "node can't be NULL");

    if (node == queue->first && node == queue->last) {
        queue->first = NULL;
        queue->last = NULL;
    } else if (node == queue->first) {
        queue->first = node->next;
        check(queue->first != NULL, "Invalid queue, somehow got a first that is NULL!");
        queue->first->prev = NULL;
    } else if (node == queue->last) {
        queue->last = node->prev;
        check(queue->last != NULL, "Invalid queue, somehow got a next that is NULL!");
        queue->last->next = NULL;
    } else {
        QueueNode *after = node->next;
        QueueNode *before = node->prev;
        after->prev = before;
        before->next = after;
    }

    queue->count--;
    result = node->value;
    free(node);

error:
    return result;
}

