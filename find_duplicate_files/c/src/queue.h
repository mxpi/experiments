#ifndef lcthw_queue_h
#define lcthw_queue_h

#include <stdlib.h>

struct QueueNode;

typedef struct QueueNode {
    struct QueueNode *next;
    struct QueueNode *prev;
    void *value;
} QueueNode;

typedef struct Queue {
    int count;
    QueueNode *first;
    QueueNode *last;
} Queue;

Queue *Queue_create();
void Queue_destroy(Queue *queue);
void Queue_clear(Queue *queue);
void Queue_clear_destroy(Queue *queue);

#define Queue_count(A) ((A)->count)
#define Queue_peek(A) ((A)-> first != NULL ? (A)->first->value : NULL)
#define Queue_last(A) ((A)-> last != NULL ? (A)->last->value : NULL)

void Queue_send(Queue *queue, void *value);
void *Queue_recv(Queue *queue);

void *Queue_remove(Queue *queue, QueueNode *node);

#define QUEUE_FOREACH(Q, C) QueueNode *C = NULL;\
for (C = (Q)->first; C != NULL; C = C->next)
#endif