#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include "bstrlib.h"
#include "darray.h"
#include <openssl/md5.h>
#include "hashmap.h"

#define INITIAL_MAX 1024

/* 
To compile:
gcc -std=gnu11 find_duplicate_files.c bstrlib.c darray.c hashmap.c -lssl -lcrypto
*/

void listdir(const char *name, int level, DArray *list)
{
    DIR *dir;
    struct dirent *entry;
    bstring curr_dir, slash;
    bstring filename;

    curr_dir = bfromcstr(name);
    slash = bfromcstr("/");

    if (!(dir = opendir(name)))
        return;
    if (!(entry = readdir(dir)))
        return;

    do {
        if (entry->d_type == DT_DIR) {
            char path[1024];
            int len = snprintf(path, sizeof(path)-1, "%s/%s", name, entry->d_name);
            path[len] = 0;
            if (   strcmp(entry->d_name, ".") == 0
                || strcmp(entry->d_name, "..") == 0
                || strcmp(entry->d_name, ".git") == 0) 
                continue;
            listdir(path, level + 1, list);
        }
        else
            if (   entry->d_type != DT_DIR
                && entry->d_type != DT_LNK 
                && entry->d_type != DT_SOCK) {
                filename = bstrcpy(curr_dir);
                bconcat(filename, slash);
                bconcat(filename, bfromcstr(entry->d_name));
                DArray_push(list, filename);
            }
    } while (entry = readdir(dir));
    closedir(dir);

    bdestroy(curr_dir);
    bdestroy(slash);
}

void display_darray(DArray *list)
{
    bstring tmp;
    for (int i = 0; i < DArray_count(list); i++) {
        tmp = DArray_get(list, i);
        printf("%s\n", bdata(tmp));
    }
}

int find_md5sum(char *md5string, char *filename)
{
    unsigned char *hash = (unsigned char *) malloc(MD5_DIGEST_LENGTH * sizeof(unsigned char));
    check_mem(hash);

    MD5_CTX mdContext;
    int bytes;
    unsigned char data[1024];
    FILE *inFile = fopen(filename, "rb");

    check(inFile != NULL, "%s can't be opened.\n", filename);

    MD5_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (hash, &mdContext);

    for(int i = 0; i < MD5_DIGEST_LENGTH; ++i)
        sprintf(&md5string[i*2], "%02x", (unsigned int)hash[i]);

    free(hash);
    fclose(inFile);

    return 0;
error:
    if (hash)
        free(hash);
    if (inFile)
        fclose(inFile);

	return -1;
}

int insert_into_hashmap(Hashmap *map, DArray *duplicates, bstring file_hash, bstring file)
{
    DArray *file_darray;

    // try to see if a darray exists for the key, otherwise spawn a new one
    file_darray = Hashmap_get(map, file_hash);
    if (file_darray == NULL) {
        file_darray = DArray_create(sizeof(DArray *), INITIAL_MAX);
    } else {
        DArray_push(duplicates, file_hash);
    }

    DArray_push(file_darray, file);

    int rc = Hashmap_set(map, file_hash, file_darray);
    check(rc == 0, "Failed to set file_hash");

    return 0;
error:
    return -1;
}

void Hashmap_destroy_custom(Hashmap *map)
{
    int i = 0;
    int j = 0;
    HashmapNode *node;

    if (map) {
        if (map->buckets) {
            for (i = 0; i < DArray_count(map->buckets); i++) {
                DArray *bucket = DArray_get(map->buckets, i);
                if (bucket) {
                    for (j = 0; j < DArray_count(bucket); j++) {
                        node = DArray_get(bucket, j);
                        if (node) {
                            DArray_destroy(node->data);
                        }
                        free(node);
                    }
                    DArray_destroy(bucket);
                }
            }
            DArray_destroy(map->buckets);
        }
        free(map);
    }
}

void DArray_destroy_custom(DArray *array)
{
    if (array) {
        bstring tmp;
        for (int i = 0; i < DArray_count(array); i++) {
            tmp = DArray_get(array, i);
            bdestroy(tmp);
        }
                
        if (array->contents)
            free(array->contents);
        free(array);
    }
}

int main(int argc, char* argv[])
{
    bstring base_dir;

    if ( argc != 2) {
        printf("Using current directory ...\n");
        char cwd[1024];
        getcwd(cwd, sizeof(cwd));
        base_dir = bfromcstr(cwd);
    } else {
        base_dir = bfromcstr(argv[1]);

        // trim the last "/" if there is one
        int len = blength(base_dir);
        if (base_dir->data[len-1] == '/') {
        	base_dir->data[len-1] = '\0';
        }
    }

    DArray *file_list = DArray_create(sizeof(DArray *), INITIAL_MAX);

    // recursively scan base_dir for files and populate file_list
    listdir(bdata(base_dir), 0, file_list);
    //display_darray(file_list);

    // create hash table
    Hashmap *map = Hashmap_create(NULL, NULL);
    check(map != NULL, "Failed to create map.");

    // create array that holds the md5 hashes of duplicate files
    DArray *duplicate_list = DArray_create(sizeof(DArray *), INITIAL_MAX);

    bstring file_path, file_hash; 
    char hash_str[33];
    for (int k = 0; k < DArray_count(file_list); k++) {
        file_path = DArray_get(file_list, k);
        find_md5sum(hash_str, bdata(file_path));
        file_hash = bfromcstralloc(33, hash_str);
        int rc = insert_into_hashmap(map, duplicate_list, file_hash, file_path);
        check(rc == 0, "Insert to hashmap failed for %s", bdata(file_path));
    }

    //extract duplicates from hash table and display
    if (DArray_count(duplicate_list) > 0) {

        Hashmap *seen = Hashmap_create(NULL, NULL);
        check(seen != NULL, "Failed to create seen map");
        
        DArray *t_darray;
        bstring md5_hash, seen_md5_hash;
        int rc;

        for (int j = 0; j < DArray_count(duplicate_list); j++) {

            md5_hash = DArray_get(duplicate_list, j);

            seen_md5_hash = Hashmap_get(seen, md5_hash);

            if (seen_md5_hash == NULL) {

                // Not seen, insert in 'seen' hash table; use anything but NULL as value (using &md5_hash here)
                rc = Hashmap_set(seen, md5_hash, &md5_hash);
                check(rc == 0, "Failed to set md5_hash");

                // fetch all duplicate files for this md5 hash
                t_darray = Hashmap_get(map, md5_hash);
                if (t_darray != NULL) {
                    printf("[");
                    bstring temp;
                    for (int i = 0; i < DArray_count(t_darray); i++) {
                        temp = DArray_get(t_darray, i);
                        printf("'%s'", bdata(temp));
                        if (i != DArray_count(t_darray)-1)
                            printf(", ");
                    }
                    printf("]\n");
                }
            }
        }
		Hashmap_destroy(seen);
    }

    bdestroy(base_dir);
    bdestroy(file_hash);
    Hashmap_destroy_custom(map);
    DArray_destroy_custom(file_list);
    DArray_destroy(duplicate_list); 

    return 0;

error:
    
    bdestroy(base_dir);
    bdestroy(file_hash);
    Hashmap_destroy_custom(map);
    DArray_destroy_custom(file_list);
    DArray_destroy(duplicate_list);

    return -1;

}
